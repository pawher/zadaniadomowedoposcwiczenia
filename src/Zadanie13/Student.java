package Zadanie13;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Student {
    long nrIndeksu;
    String imie;
    String nazwisko;
    List<Integer> listaOcen;

    public Student(long nrIndeksu, String imie, String nazwisko, List<Integer> listaOcen) {

        this.nrIndeksu = nrIndeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.listaOcen = listaOcen;
    }

    public static int dodajOcene () {
        int ocena;
        Random random = new Random();
        ocena = random.nextInt(5);
        return ocena +1;
    }

    public static List<Integer> generatorOcen(int iloscOcen){
        List<Integer > lista = new ArrayList<>();
        for (int i = 0; i < iloscOcen; i++) {
            lista.add(dodajOcene());
        }

        return lista;
    }

    public long getNrIndeksu() {
        return nrIndeksu;
    }

    public void setNrIndeksu(long nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public List<Integer> getListaOcen() {
        return listaOcen;
    }

    public void setListaOcen(List<Integer> listaOcen) {
        this.listaOcen = listaOcen;
    }

    public static double srednia (List<Integer> lista) {
        double suma = 0;
        for (int i = 0; i <lista.size() ; i++) {
            suma = lista.get(i) + suma;
        }
        return suma/lista.size();
    }

    public static boolean testOcen (List<Integer> lista) {

        for (int i = 0; i <lista.size(); i++) {
            if (lista.get(i) == 1 || lista.get(i) == 2) {
                return true;
            }
        }
             return false;
    }
}
