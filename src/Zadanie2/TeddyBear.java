package Zadanie2;

public class TeddyBear {
    private String name;

    public TeddyBear(String name) {
        this.name = name;
    }

    protected String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void printName () {
        System.out.println("Jestem miś " + getName());
    }
}
