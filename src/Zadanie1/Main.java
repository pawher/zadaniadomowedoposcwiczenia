package Zadanie1;

public class Main {
    public static void main(String[] args) {
        Osoba janek = new Osoba("Janek", 45);
        Osoba anna = new Osoba("Anna", 33);
        Osoba alicja = new Osoba("Alicja", 29);

        janek.printYourNameAndAge();
        anna.printYourNameAndAge();
        alicja.printYourNameAndAge();

        System.out.println(janek);

        System.out.println(janek.getImie()+ " " + janek.getWiek());

    }
}
