package Zadanie12;

public class Square {
    int a;

    public Square(int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public static double obliczPoleKwadratu (int a) {
        return a*a;
    }

    public static double obliczObwodKwadratu (int a) {
        return 4*a;
    }
}
