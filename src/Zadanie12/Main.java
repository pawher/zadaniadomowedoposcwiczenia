package Zadanie12;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        Circle kolo = new Circle(4);
//        Square kwadrat = new Square(5);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj figure: [kolo / kwadrat]");
        String figura = scanner.nextLine();

        System.out.println("Podaj bok/promien: ");
        int bok = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Podaj dzialanie: [pole/obwod]");
        String dzialanie = scanner.nextLine();

        if (figura.equals("kolo") && dzialanie.equals("pole"))
        {
            System.out.println(Circle.obliczPoleKola(bok));

        } else if (figura.equals("kolo") && dzialanie.equals("obwod")) {
            System.out.println(Circle.obliczObwodKola(bok));
        } else if (figura.equals("kwadrat") && dzialanie.equals("pole")) {
            System.out.println(Square.obliczPoleKwadratu(bok));
        } else {
            System.out.println(Square.obliczObwodKwadratu(bok));
        }



    }
}
