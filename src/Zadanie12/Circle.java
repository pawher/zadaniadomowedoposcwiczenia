package Zadanie12;

public class Circle {
    int promien;

    public Circle(int promien) {
        this.promien = promien;
    }

    public int getPromien() {
        return promien;
    }

    public void setPromien(int promien) {
        this.promien = promien;
    }

    public static double obliczPoleKola (int r) {
        return Math.PI*r*r;
    }

    public static double obliczObwodKola (int r) {
        return 2*Math.PI*r;
    }
}
