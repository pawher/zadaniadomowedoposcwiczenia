package Zadanie14;

import Zadanie11.OperacjeList;

public class Paczka {
    String odbiorca;
    String nadawca;
    boolean czyWyslana;
    boolean czyJestZawartosc;

    public Paczka(String odbiorca, String nadawca, boolean czyWyslana, boolean czyJestZawartosc) {
        this.odbiorca = odbiorca;
        this.nadawca = nadawca;
        this.czyWyslana = czyWyslana;
        this.czyJestZawartosc = czyJestZawartosc;
    }
    public  Paczka () {}

    public String getOdbiorca() {
        return odbiorca;
    }

    public void setOdbiorca(String odbiorca) {
        this.odbiorca = odbiorca;
    }

    public String getNadawca() {
        return nadawca;
    }

    public void setNadawca(String nadawca) {
        this.nadawca = nadawca;
    }

    public boolean isCzyWyslana() {
        return czyWyslana;
    }

    public void setCzyWyslana(boolean czyWyslana) {
        this.czyWyslana = czyWyslana;
    }

    @Override
    public String toString() {
        return "Paczka{" +
                "odbiorca='" + odbiorca + '\'' +
                ", nadawca='" + nadawca + '\'' +
                ", czyWyslana=" + czyWyslana +
                ", czyJestZawartosc=" + czyJestZawartosc +
                '}';
    }

    public boolean isCzyJestZawartosc() {
        return czyJestZawartosc;
    }

    public void setCzyJestZawartosc(boolean czyJestZawartosc) {
        this.czyJestZawartosc = czyJestZawartosc;
    }

    public void wyslij () {
        if (odbiorca != null && czyJestZawartosc == true) {
            czyWyslana = true;
        System.out.println("paczka wyslana");

        }

    }
    public void wyslijPrio () {
        if (odbiorca != null && czyJestZawartosc == true && nadawca != null && czyWyslana != true) {
            czyWyslana = true;
            System.out.println("paczka wyslana priorytetem");

        }

    }
}
