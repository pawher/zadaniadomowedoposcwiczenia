package Zadanie4;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Calculator cal = new Calculator();
//
//        System.out.println(cal.addTwoNumbers(3,5));
//        System.out.println(cal.multiplyTwoNumbers(5,6));
//        System.out.println(cal.divideTwoNumbers(35,5));

        Scanner scr = new Scanner(System.in);
        System.out.println("Podaj dzialanie [dodaj, odejmij, pomnoz, podziel, quit] oraz dwie liczby");
        String wejscie = scr.nextLine();
        String[] linia = wejscie.split(" ");
        String polecenie = linia[0];


        if (polecenie.equals("quit")) {
            System.out.println("koniec");
            return;

        }
        int a1 = Integer.parseInt(linia[1]);

        int b1 = Integer.parseInt(linia[2]);


        if (polecenie.equals("dodaj")) {
            System.out.println("Wynik dodawania " + a1 + " i " + b1 + " wynosi: " + cal.addTwoNumbers(a1, b1));


        }
        if (polecenie.equals("odejmi")) {
            System.out.println("Wynik odejmowania " + b1 + " od " + a1 + " wynosi: " + cal.substractTwoNumbers(a1, b1));
        }
        if (polecenie.equals("pomnoz")) {
            System.out.println("Wynik mnożenia " + a1 + " i " + b1 + " wynosi: " + cal.multiplyTwoNumbers(a1, b1));
        }
        if (polecenie.equals("podziel")) {
            System.out.println("Wynik dzielenia " + a1 + " przez " + b1 + " wynosi: " + cal.divideTwoNumbers(a1, b1));
        }


    }
}

