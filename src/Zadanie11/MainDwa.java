package Zadanie11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainDwa {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>(Arrays.asList(1, 2, 4, 2, 5, 12, 3, 2));
        int[] tablica = {4, 2, 2, 1, 5, 29, 3, 8};
        List<Integer> lista1 = new ArrayList<>(Arrays.asList(5, 2, 5, 2, 5, 12, 3, 2));

//        int counter = 0;
//        for (int i = 0; i < 8; i++) {
//            if (lista.get(i) == tablica[i]) {
//                counter++;
//            }
//        }
//        System.out.println(counter);
//
//        System.out.println(copiaListy(lista));
//        System.out.println(Arrays.toString(przepiszListe(lista)));
//
//        System.out.println(przepiszTablice(tablica));
//        System.out.println(polaczListy(lista1, lista));
//
//        System.out.println(polaczDowolneListy(lista, lista1, lista));
//        System.out.println(polaczGenerycznie(lista,lista,lista1));
        System.out.println(sprawdzListy(lista1, lista));


    }

    public static List<Integer> copiaListy(List<Integer> lista) {
        List<Integer> copia = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            int c = lista.get(i);
            copia.add(i, c);
        }
        return copia;
    }

    public static int[] przepiszListe(List<Integer> lista) {
        int[] copia = new int[lista.size()];
        for (int i = 0; i < lista.size(); i++) {
            int c = lista.get(i);
            copia[i] = c;

        }
        return copia;
    }

    public static List<Integer> przepiszTablice(int[] tablica) {
        List<Integer> copia = new ArrayList<>();
        for (int i = 0; i < tablica.length; i++) {
            int c = tablica[i];
            copia.add(i, c);

        }
        return copia;
    }

    public static List<Integer> polaczListy(List<Integer> lista1, List<Integer> lista2) {
        List<Integer> copia = new ArrayList<>();
        copia.addAll(lista1);
        copia.addAll(lista2);
        return copia;
    }

    public static List<Integer> polaczDowolneListy(List<Integer>... listas) {
        List<Integer> copia = new ArrayList<>();
        for (List<Integer> listaZList : listas) {
            copia.addAll(listaZList);
        }
        return copia;

    }

//    public static List<Integer> polaczGenerycznie(List<Integer>... listass) {
//        List<Integer> copia = new ArrayList<>();
//        for (int i = 0; i < listass.length; i++) {
//            copia.addAll(polaczGenerycznie(listass));
//        }
//        return copia;
//
//    }

    public static boolean sprawdzListy(List<Integer> lista1, List<Integer> lista2) {
//        boolean test = true;
        for (int i = 0; i < lista2.size(); i++) {
            if (lista1.get(i) != lista2.get(i)) {
                return false;
            }
        }
        return true;

    }
    public static boolean sprawdzListy2(List<Integer> lista1, List<Integer> lista2) {
        boolean test = true;
        for (int i = 0; i < lista2.size(); i++) {
            if (lista1.get(i) != lista2.get(i)) {
                test = false;
                break;
            }
        }
        return test;

    }


}



