package Zadanie8;

public class QuadraticEquation {
    double a;
    double b;
    double c;

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double calculateDelta() {
        double delta = (b * b) - (4 * a * c);
        return delta;

    }

    public double calculateX1() {
        if (calculateDelta() > 0) {
            double pierwiastek = Math.sqrt(calculateDelta());
            double x1 = (-b - pierwiastek) / (2 * a);
            return x1;

        } else if (calculateDelta() < 0) {
            throw new ArithmeticException("brak pierwiastkow");
//
//                System.out.println("barak pierwiastkow");

        } else {
            double x0 = (-b) / (2 * a);
            return x0;
        }
    }



    public double calculateX2() {
        if (calculateDelta() > 0) {
            double pierwiastek = Math.sqrt(calculateDelta());
            double x2 = (-b + pierwiastek) / (2 * a);
            return x2;
        } else if (calculateDelta() < 0) {
            throw new DeltaLessThanZeroException();
//
//                System.out.println("barak pierwiastkow");

        } else  {
            double x0 = (-b) / (2 * a);
            return x0;

        }


        }
    }
