package Zadanie3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Dziennik {
    private String[] studentList = new String[30];
    private int studentCount;
//    private List<String> studentLIst = new ArrayList<>();

    public Dziennik(String[] studentList) {
        this.studentList = studentList;

    }

    public Dziennik() {
        this.studentCount = 0;
    }

    public void addStudent (String name) {
        studentList [studentCount++] = name;
    }

    public void removeStudent () {
        studentCount--;
    }

    public void printStudensts () {
        for (int i = 0; i < studentCount; i++) {
            System.out.println(studentList[i]);
        }
    }

    @Override
    public String toString() {
        return "Dziennik{" +
                "studentList=" + Arrays.toString(studentList) +
                ", studentCount=" + studentCount +
                '}';
    }

    public String[] getStudentList() {
        return studentList;
    }

    public void setStudentList(String[] studentList) {
        this.studentList = studentList;
    }

    public int getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(int studentCount) {
        this.studentCount = studentCount;
    }
}
